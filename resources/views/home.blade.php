@extends('layouts.app', ['title' => 'Home'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Manager Name</th>
                                <th scope="col">Manager Location</th>
                                <th scope="col">Assigned Driver</th>
                                <th scope="col">Status</th>
                                <th scope="col">Request Date</th>
                                <th scope="col">Request Type</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['garbage'] as $key => $loc)
                            <?php
                            $status = '';
                            if ($loc->status == 0) {
                                $status = 'Unread';
                            } else if ($loc->status == 1) {
                                $status = 'Assigned';
                            } else {
                                $status = 'Completed';
                            }
                            ?>
                            <tr>
                                <th scope="row">
                                    {{$key + 1}}</th>
                                <td>{{$loc->manager_name}}</td>
                                <td>{{$loc->location_name}}</td>
                                <td>
                                    @if($loc->driver_name)
                                    {{$loc->driver_name}}
                                    @else
                                    Unassigned
                                    @endif
                                </td>
                                <td>{{$status}}</td>
                                <td>{{$loc->created_at}}</td>
                                <td>
                                    <svg width="100" height="100">
                                        <rect x="30" y="10" width="20" height="20" style="fill:{{$loc->type}};" />
                                    </svg>
                                </td>
                                <th scope="col">
                                    @if(!$loc->driver_name)
                                    <a href="/driver_assign/{{$loc->id}}">Assign Driver</a>
                                    @else
                                    @if($loc->status != 2 )
                                    <form method="post" action="{{route('complete_collection')}}">
                                        @csrf
                                        <input type="hidden" name="garbage_id" value="{{$loc->id}}" />
                                        <input type="hidden" name="driver_id" value="{{$loc->driver_id}}" />
                                        <button type="submit" class="btn btn-primary">
                                            Complete Collection
                                        </button>
                                    </form>
                                    @else
                                    Completed
                                    @endif
                                    @endif
                                </th>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection