@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Assign Driver</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('assign_driver') }}">
                        @csrf
                        <input type="hidden" name="garbage_id" value="{{$data['garbage_id']}}" />
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Driver Name</label>

                            <div class="col-md-6">
                                <select class="form-control" name="driver_id">
                                    @foreach($data['drivers'] as $driver)
                                    <option value="{{$driver->id}}">{{$driver->name}}</option>
                        @endforeach
                        </select>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Assign Driver
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

</div>

@endsection