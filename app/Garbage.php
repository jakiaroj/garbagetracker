<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Garbage extends Model
{
    public function getRequests()
    {
        $garbage = DB::table('garbage_collection')
            ->join('users as  m', 'm.id', 'garbage_collection.manager_id')
            ->leftJoin('users as d', 'd.id', 'garbage_collection.driver_id')
            ->join('locations', 'garbage_collection.location_id', 'locations.location_id')
            ->select([
                'garbage_collection.garbage_id as id',
                'garbage_collection.message as message',
                'garbage_collection.type as type',
                'garbage_collection.status as status',
                'location_name',
                'garbage_collection.created_at as created_at',
                'm.name as manager_name',
                'd.name as driver_name',
                'd.id as driver_id'
            ])
            ->orderBy('created_at', 'desc')
            ->get();
        return $garbage;
    }

    public function getInactiveDrivers()
    {
        $drivers = DB::table('users')->where('user_type', 1)->get();
        return $drivers;
    }

    public function assignDriver($data, $garbage_id)
    {
        DB::table('garbage_collection')->where('garbage_id', $garbage_id)->update($data);
    }

    public function driverActiveJobs($driver_id)
    {
        $garbage = DB::table('garbage_collection')
            ->join('users as  m', 'm.id', 'garbage_collection.manager_id')
            ->leftJoin('users as d', 'd.id', 'garbage_collection.driver_id')
            ->join('locations', 'garbage_collection.location_id', 'locations.location_id')
            ->select([
                'garbage_collection.garbage_id as id',
                'garbage_collection.message as message',
                'garbage_collection.type as type',
                'garbage_collection.status as status',
                'location_name',
                'garbage_collection.created_at as created_at',
                'm.name as manager_name',
                'd.name as driver_name',
                'd.id as driver_id'
            ])
            ->where('garbage_collection.driver_id', $driver_id)
            ->where('garbage_collection.status', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        return $garbage;
    }

    public function createRequest($data)
    {
        DB::table('garbage_collection')->insert($data);
    }

    public function managerHistory($manager_id)
    {
        $garbage = DB::table('garbage_collection')
            ->join('users as  m', 'm.id', 'garbage_collection.manager_id')
            ->leftJoin('users as d', 'd.id', 'garbage_collection.driver_id')
            ->join('locations', 'garbage_collection.location_id', 'locations.location_id')
            ->select([
                'garbage_collection.garbage_id as id',
                'garbage_collection.message as message',
                'garbage_collection.type as type',
                'garbage_collection.status as status',
                'location_name',
                'garbage_collection.created_at as created_at',
                'm.name as manager_name',
                'd.name as driver_name',
                'd.id as driver_id'
            ])
            ->where('garbage_collection.manager_id', $manager_id)
            ->orderBy('created_at', 'desc')
            ->get();
        return $garbage;
    }

    public function completeAction($manager_id, $driver_id)
    {
        $data = array(
            "status" => 2
        );
        DB::table('garbage_collection')
            ->where('manager_id', $manager_id)
            ->where('driver_id', $driver_id)
            ->where('status', 1)
            ->update($data);
    }

    public function driverJobPending($driver_id)
    {
        $exists = DB::table('garbage_collection')->where('driver_id', $driver_id)->where('status', 1)->exists();
        return $exists;
    }


    public function driverJobHistory($driver_id)
    {
        $garbage = DB::table('garbage_collection')
            ->join('users as  m', 'm.id', 'garbage_collection.manager_id')
            ->leftJoin('users as d', 'd.id', 'garbage_collection.driver_id')
            ->join('locations', 'garbage_collection.location_id', 'locations.location_id')
            ->select([
                'garbage_collection.garbage_id as id',
                'garbage_collection.message as message',
                'garbage_collection.type as type',
                'garbage_collection.status as status',
                'location_name',
                'garbage_collection.created_at as created_at',
                'm.name as manager_name',
                'd.name as driver_name',
                'd.id as driver_id'
            ])
            ->where('garbage_collection.driver_id', $driver_id)
            ->orderBy('created_at', 'desc')
            ->get();
        return $garbage;
    }

    public function checkManager($manager_id) {
        $check = DB::table('users')->where('id', $manager_id)->exists();
        return $check;
    }
}
