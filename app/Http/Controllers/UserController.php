<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Manager;
use App\Garbage;
use App\Driver;

class UserController extends Controller
{

    public function __construct()
    {
        $this->manager = new Manager();
        $this->garbage = new Garbage();
        $this->driver = new Driver();
    }

    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            if ($user->user_type == 0) {
                return response()->json(['error' => 'Unauthorised'], 401);
            } else if ($user->user_type == 2) {
                $location = $this->manager->getManagerAssign($user->id);
                $user['location'] = $location->location_name;
                $user['location_id'] = $location->location_id;
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_data'] = $user;
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user_data'] = $user;
                return response()->json(['success' => $success], $this->successStatus);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'user_type' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if ($user->user_type == 2) {
            $data = array(
                "user_id" => $user->id,
                "location_id" => $request->input('location_id')
            );
            $this->manager->assignUser($data);
        }
        //$success['token'] =  $user->createToken('MyApp')->accessToken;
        //$success['name'] =  $user->name;
        //dd($user->id);
        return back();
    }
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function details()
    {
        $user = Auth::user();
        //dd($user);
        if ($user->user_type == 2) {
            $location = $this->manager->getManagerAssign($user->id);
            $user['location'] = $location->location_name;
            $user['location_id'] = $location->location_id;
            $success['user_data'] = $user;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            $success['user_data'] = $user;
            return response()->json(['success' => $success], $this->successStatus);
        }
    }

    public function logout()
    {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);
        $accessToken->revoke();
        return response()->json(['success' => "Logged Out"], 200);
    }

    public function driver_active_jobs()
    {
        $user = Auth::user();
        $jobs = $this->garbage->driverActiveJobs($user->id);
        return response()->json(['success' => $jobs], $this->successStatus);
    }

    public function driver_job_history()
    {
        $user = Auth::user();
        $jobs = $this->garbage->driverJobHistory($user->id);
        return response()->json(['success' => $jobs], $this->successStatus);
    }

    public function create_collection_request(Request $request)
    {
        $input = $request->all();
        $this->garbage->createRequest($input);
        return response()->json(['success' => "Request Created"], $this->successStatus);
    }

    public function manager_history()
    {
        $user = Auth::user();
        $history = $this->garbage->managerHistory($user->id);
        return response()->json(['success' => $history], $this->successStatus);
    }

    public function complete_alert(Request $request)
    {
        $user = Auth::user();
        $driver_id = $user->id;
        $input = $request->all();
        $manager_id = $input['manager_id'];
        $checkManager = $this->garbage->checkManager($manager_id);

        if ($checkManager) {
            $this->garbage->completeAction($manager_id, $driver_id);
            $exists = $this->garbage->driverJobPending($driver_id);
            if ($exists == false) {
                $driverData = array(
                    "user_status" => 0
                );
                $this->driver->updateDriverStatus($driver_id, $driverData);
            }
            return response()->json(['success' => "Alert Status Completed"], $this->successStatus);
        } else {
            return response()->json(['error' => "Manager not found"], 400);
        }
    }
}
