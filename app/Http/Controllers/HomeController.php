<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Locations;
use App\Driver;
use App\Manager;
use App\Garbage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->location = new Locations();
        $this->driver = new Driver();
        $this->manager = new Manager();
        $this->garbage = new Garbage();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $garbage = $this->garbage->getRequests();
        //dd($garbage);
        $data = array(
            "garbage" => $garbage
        );
        //dd(Auth::user());
        return view('home')->with("data", $data);
    }

    public function driver()
    {
        $driver = $this->driver->getDrivers();
        $data = array(
            "driver" => $driver
        );
        return view('driver')->with("data", $data);
    }

    public function manager()
    {
        $manager = $this->manager->getManager();
        $locations = $this->location->getLocations();
        $data = array(
            "location_name" => $locations,
            "manager" => $manager
        );
        return view('manager')->with("data", $data);
    }

    public function locations()
    {
        $locations = $this->location->getLocations();
        $data = array(
            "locations" => $locations
        );
        return view('locations')->with("data", $data);
    }

    public function create_locations(Request $request)
    {
        $location_name = $request->input('location_name');
        $data = array(
            "location_name" => $location_name
        );
        $this->location->createLocation($data);
        return back();
    }

    public function assign_driver(Request $request)
    {
        $driver_id =  $request->input('driver_id');
        $garbage_id = $request->input('garbage_id');
        $driverData = array(
            "user_status" => 1
        );
        $this->driver->updateDriverStatus($driver_id, $driverData);

        $data = array(
            "driver_id" => $driver_id,
            "status" => 1
        );

        $this->garbage->assignDriver($data, $garbage_id);
        return redirect()->route('home');
    }

    public function driver_assign_page($garbage_id)
    {
        $drivers = $this->garbage->getInactiveDrivers();
        $data = array(
            "drivers" => $drivers,
            "garbage_id" => $garbage_id
        );
        return view('driver_assign')->with("data", $data);
    }

    public function complete_collection(Request $request)
    {
        $garbage_id = $request->input('garbage_id');
        $driver_id = $request->input('driver_id');
        $driverData = array(
            "user_status" => 0
        );
        $this->driver->updateDriverStatus($driver_id, $driverData);

        $data = array(
            "status" => 2
        );
        $this->garbage->assignDriver($data, $garbage_id);
        return back();
    }


}
