<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Locations extends Model
{
    public $table = 'locations';

    public function createLocation($data)
    {
        DB::table('locations')->insert($data);
    }

    public function getLocations()
    {
        $locations = DB::table('locations')->orderBy('location_id', 'desc')->get();
        return $locations;
    }
}
