<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Manager extends Model
{
    public $table = 'assigned_location';

    public function assignUser($data)
    {
        DB::table('assigned_location')->insert($data);
    }

    public function getManager()
    {
        $manager = DB::table('assigned_location')
            ->join('users', 'user_id', 'users.id')
            ->join('locations', 'assigned_location.location_id', 'locations.location_id')
            ->get();
        return $manager;
    }

    public function getManagerAssign($user_id){
        $manager = DB::table('assigned_location')
        ->join('locations', 'assigned_location.location_id', 'locations.location_id')
        ->where('assigned_location.user_id', $user_id)
        ->first();
    return $manager;
    }
}
