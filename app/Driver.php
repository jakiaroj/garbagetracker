<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Driver extends Model
{
    public function getDrivers()
    {
        $driver = DB::table('users')->where('user_type', 1)->orderBy('user_status', 'asc')->get();
        return $driver;
    }

    public function updateDriverStatus($driver_id, $data)
    {

        DB::table('users')->where('id', $driver_id)->update($data);
    }
}
