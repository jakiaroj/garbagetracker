<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/driver', 'HomeController@driver')->name('driver')->middleware('auth');
Route::get('/manager', 'HomeController@manager')->name('manager')->middleware('auth');
Route::get('/locations', 'HomeController@locations')->name('locations')->middleware('auth');
Route::post('/create_locations', 'HomeController@create_locations')->name('create_locations')->middleware('auth');
Route::post('/assign_driver', 'HomeController@assign_driver')->name('assign_driver')->middleware('auth');
Route::get('/driver_assign/{id}', 'HomeController@driver_assign_page')->name('driver_assign')->middleware('auth');
Route::post('/complete_collection', 'HomeController@complete_collection')->name('complete_collection')->middleware('auth');

//auth
Route::post('register_driver', 'UserController@register')->name('register_driver')->middleware('auth');




//API Routes
Route::group(array('prefix' => 'api'), function () {
    Route::post('login', 'UserController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('details', 'UserController@details');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'UserController@logout');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('driver_active_jobs', 'UserController@driver_active_jobs');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('driver_job_history', 'UserController@driver_job_history');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('create_collection_request', 'UserController@create_collection_request');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('manager_history', 'UserController@manager_history');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('complete_alert', 'UserController@complete_alert');
    });
});
